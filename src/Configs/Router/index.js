import { createBrowserRouter } from 'react-router-dom';

// import Home from 'src/Screens/Home';

import Home from 'src/Screens/HomeBackup';
import Birthday1 from 'src/Containers/Birthday1';
import Birthday2 from 'src/Containers/Birthday2';
import Birthday3 from 'src/Containers/Birthday3';

import BirthdayLayout from 'src/Screens/BirthdayLayout';
import Test from '../../Screens/Test';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />,
    errorElement: <Home />,
  },
  {
    path: '/:id/:index',
    element: <Birthday1 />,
    errorElement: <Birthday1 />,
  },
  {
    path: '/test',
    element: <Test />,
    errorElement: <Test />,
  },
  {
    path: '/birthday',
    element: <BirthdayLayout />,
    children: [
      {
        path: '/birthday/1',
        element: <Birthday1 />,
      },
      {
        path: '/birthday/2',
        element: <Birthday2 />,
      },
      {
        path: '/birthday/3',
        element: <Birthday3 />,
      },
    ],
  },
]);

export default router;
