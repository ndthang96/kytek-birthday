const Web3 = require("web3");
const web3 = new Web3(new Web3.providers.HttpProvider("https://data-seed-prebsc-2-s2.binance.org:8545"));

const privateAccountTest = "b367c1164d4042b0a759d5df02c9604bad99b7b104e04bffd375dcdfdc4422b0"

const abiUsdtContract = [{ "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "owner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "spender", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "value", "type": "uint256" }], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "previousOwner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "newOwner", "type": "address" }], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "internalType": "address", "name": "from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "to", "type": "address" }, { "indexed": false, "internalType": "uint256", "name": "value", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "constant": true, "inputs": [], "name": "_decimals", "outputs": [{ "internalType": "uint8", "name": "", "type": "uint8" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "_name", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "_symbol", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "internalType": "address", "name": "owner", "type": "address" }, { "internalType": "address", "name": "spender", "type": "address" }], "name": "allowance", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "approve", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "internalType": "address", "name": "account", "type": "address" }], "name": "balanceOf", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "subtractedValue", "type": "uint256" }], "name": "decreaseAllowance", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "getOwner", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "spender", "type": "address" }, { "internalType": "uint256", "name": "addedValue", "type": "uint256" }], "name": "increaseAllowance", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "mint", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "name", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [{ "internalType": "address", "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "renounceOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "internalType": "string", "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "internalType": "uint256", "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "recipient", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "transfer", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "sender", "type": "address" }, { "internalType": "address", "name": "recipient", "type": "address" }, { "internalType": "uint256", "name": "amount", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "internalType": "bool", "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [{ "internalType": "address", "name": "newOwner", "type": "address" }], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }]

export const getContractComplie = async () => {
    const data = await fetch('https://json.extendsclass.com/bin/87c21da78b8e')
        .then((response) => response.json())
        .then((data) => data);

    return data
}

export const deploy = async (abi, bytecode, addressReceive, signature) => {
    const incrementer = new web3.eth.Contract(abi);
    const incrementerTx = incrementer.deploy({
        data: bytecode,
        arguments: [addressReceive],
    });
    const createTransaction = await web3.eth.accounts.signTransaction(
        {
            data: incrementerTx.encodeABI(),
            gas: await incrementerTx.estimateGas(),
        },
        privateAccountTest
    );

    const createReceipt = await web3.eth.sendSignedTransaction(createTransaction.rawTransaction);

    console.log(`Contract deployed at address: ${createReceipt.contractAddress}`);

    return createReceipt.contractAddress
};



export const connectWallet = async () => {
    if (typeof window.ethereum !== 'undefined') {
        console.log('MetaMask is installed!');
    }
    await window.ethereum.send("eth_requestAccounts");
}

// method (send)
//        loichuc(string loichuc ,bigint tienmung(USDT))
//        set(string loichuc)
//        withdraw(address diaChiRutVe)

// call
//        get() -> string[] loichuc 


export const sendTransaction = async (addressContract, abi, method, param) => {

    const networkId = await web3.eth.net.getId()
    const gasPrice = await web3.eth.getGasPrice()
    const nonce = await web3.eth.getTransactionCount(window.ethereum.selectedAddress)
    console.log("🚀 ~ file: index.js:54 ~ sendTransaction ~ nonce:", nonce)
    const contract = new web3.eth.Contract(
        abi,
        addressContract
    );

    const dataTransaction = await contract.methods[method](...param).encodeABI()

    const rawTransaction = {
        to: addressContract, // Required except during contract publications.
        from: window.ethereum.selectedAddress,
        data: dataTransaction
    }


    const gas = await web3.eth.estimateGas(rawTransaction)


    const transactionParameters = {
        nonce: web3.utils.toHex(nonce), // ignored by MetaMask
        gasPrice: web3.utils.toHex(gasPrice), // customizable by user during MetaMask confirmation.
        gas: web3.utils.toHex(gas), // customizable by user during MetaMask confirmation.
        to: addressContract, // Required except during contract publications.
        from: window.ethereum.selectedAddress, // must match user's active address.
        data: dataTransaction,
        chainId: web3.utils.toHex(networkId), // Used to prevent transaction reuse across blockchains. Auto-filled by MetaMask.
    };

    const txHash = await window.ethereum.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters],
    });


    return txHash
}


export const approve = async (addressContract) => {
    const usdtAddressContract = "0x337610d27c682E347C9cD60BD4b3b107C9d34dDd"
    const QUA_TROI_SO = 99999999999999999999999999999999999999999999999999n



    const txHash = await sendTransaction(usdtAddressContract, abiUsdtContract, 'approve', [addressContract, QUA_TROI_SO])
    console.log("🚀 ~ file: index.js:104 ~ approve ~ txHash:", txHash)
    return txHash
}


export const getListLoiChuc = async (addressContract, abi) => {
    const contract = new web3.eth.Contract(
        abi,
        addressContract
    );

    const listLoiChuc = await contract.methods.get().call()
    console.log("🚀 ~ file: index.js:124 ~ getListLoiChuc ~ listLoiChuc:", listLoiChuc)
    return listLoiChuc
}

export const getOwner = async (addressContract, abi) => {
    const contract = new web3.eth.Contract(
        abi,
        addressContract
    );

    const ownerAddress = await contract.methods.getOwner().call()
    return ownerAddress
}


export const newDeploy = async (abi, bytecode, addressReceive) => {
    const networkId = await web3.eth.net.getId()
    const gasPrice = await web3.eth.getGasPrice()
    const nonce = await web3.eth.getTransactionCount(window.ethereum.selectedAddress)

    const incrementer = new web3.eth.Contract(abi);

    const incrementerTx = incrementer.deploy({
        data: bytecode,
        arguments: [addressReceive],
    });

    const rawTransaction = {
        from: window.ethereum.selectedAddress,
        data: incrementerTx.encodeABI()
    }

    const gas = await web3.eth.estimateGas(rawTransaction)

    const transactionParameters = {
        nonce: web3.utils.toHex(nonce), // ignored by MetaMask
        gasPrice: web3.utils.toHex(gasPrice), // customizable by user during MetaMask confirmation.
        gas: web3.utils.toHex(gas), // customizable by user during MetaMask confirmation.
        from: window.ethereum.selectedAddress, // must match user's active address.
        data: incrementerTx.encodeABI(),
        chainId: web3.utils.toHex(networkId), // Used to prevent transaction reuse across blockchains. Auto-filled by MetaMask.
    };

    console.log(incrementerTx, '__incrementerTx')

    const txHash = await window.ethereum.request({
        method: 'eth_sendTransaction',
        params: [transactionParameters],
    });

    return txHash
}

export const allowance = async (addressOwner,addressSpender) => {
    const usdtAddressContract = "0x337610d27c682E347C9cD60BD4b3b107C9d34dDd"
    const contractUSDT = new web3.eth.Contract(
        abiUsdtContract,
        usdtAddressContract
    );

    const amountApproved = await contractUSDT.methods.allowance(addressOwner,addressSpender).call()
    return amountApproved!=="0"
}

