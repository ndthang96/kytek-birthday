import React, { memo, useEffect, useState } from 'react';
import { EffectCoverflow, Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import {
  getContractComplie,
  deploy,
  newDeploy,
  sendTransaction,
  approve,
  getListLoiChuc,
  allowance,
} from '../../Contract';
import Web3 from 'web3';
import { useForm } from 'react-hook-form';
import loading from '../../images/Infinity-1s-200px.gif';

import 'swiper/css';
import 'swiper/css/effect-coverflow';
import 'swiper/css/pagination';

import './home.scss';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import 'sweetalert2/src/sweetalert2.scss';

const HOMES = [
  {
    title: 'Birthday Layout',
    isComing: false,
    img: '/home/birthday.jpg',
  },
  {
    title: 'Christmas Layout',
    isComing: true,
    img: '/home/christmas.jpeg',
  },
  {
    title: 'Mid Autumn Layout',
    isComing: true,
    img: '/home/mid_autumn.avif',
  },
  {
    title: 'New Year Layout',
    isComing: true,
    img: '/home/new_year.avif',
  },
  {
    title: 'Valentine Layout',
    isComing: true,
    img: '/home/valentine.avif',
  },
];

const customAlert = Swal.mixin({
  buttonsStyling: false,
});

const Home = ({ className }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
    setValue,
    watch,
  } = useForm();
  const web3 = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545'); // Node BSC testnet

  const ethereum = window.ethereum;
  const [state, set] = useState({
    isOpen: false,
    loading: false,
    isShowForm: false,
    accountConnect: '',
    addressContract: '',
    isConnectContract: false,
  });

  const setState = (newState) => {
    set((currentState) => ({
      ...currentState,
      ...newState,
    }));
  };

  const [isApproved, setIsApproved] = useState(false);

  const addressContract = watch('addressContract', false);

  const toggleForm = () => setState({ isShowForm: !state.isShowForm });

  const openModal = () => setState({ isOpen: true });

  const closeModal = () => setState({ isOpen: false });

  const preventClick = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const copyToClipBoard = async (content = '') => {
    if (typeof window !== 'undefined') {
      if (navigator.clipboard && window.isSecureContext) {
        try {
          await navigator.clipboard.writeText(content);

          Swal.fire({
            timer: 1500,
            icon: 'success',
            position: 'top-end',
            showConfirmButton: false,
            title: 'Sao chép thành công',
          });
        } catch (err) {}
      } else {
        const textArea = document.createElement('textarea');
        textArea.value = content;

        textArea.style.position = 'fixed';
        textArea.style.left = '-999999px';
        textArea.style.top = '-999999px';
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
          await document.execCommand('copy');

          Swal.fire({
            timer: 1500,
            icon: 'success',
            position: 'top-end',
            showConfirmButton: false,
            title: 'Sao chép thành công',
          });
        } catch (err) {}

        textArea.remove();
      }
    }
  };

  const onSubmitForm = async (data) => {
    // event.preventDefault();
    // event.stopPropagation();
    if (!state.isConnectContract) {
      onClickBuy();
    } else if (!isApproved) {
      const txHashApprove = await approve(data.addressContract);
      console.log(txHashApprove, 'txHashApprove');
      setIsApproved(true);
    } else {
      //test gui loi chuc + tien mung
      const ecec = await getContractComplie();

      const dataSend = {
        name: getValues('name'),
        index: 1,
        loichuc: getValues('loichuc'),
        layout: 1,
      };

      const dataSendTrans = JSON.stringify(dataSend);

      const { abi, bytecode } = ecec;
      const txHash = await sendTransaction(
        data.addressContract,
        abi,
        'loichuc',
        [dataSendTrans, getValues('tienmung')]
      );

      // const getLoi = await getListLoiChuc(data.addressContract, abi);
      // console.log(getLoi, '__getLoi');
      console.log(txHash, '__txHash');
// <<<<<<< HEAD
//       // const listLoiChuc = await getListLoiChuc(data.addressContract, abi);
//       // console.log(listLoiChuc, '__listLoiChuc')
//       alert(
//         `Đã gửi thành công coppy link http://localhost:3000/${data.addressContract}/0`
//       );
// =======

      customAlert
        .fire({
          icon: 'success',
          title: 'Đã gửi thành công',
          html: `Sao chép đường dẫn<br /><span onClick="">http://localhost:3000/${data.addressContract}/0</span><br />và chia sẻ cho bạn bè`,
          confirmButtonText: 'Copy link',
        })
        .then((result) => {
          if (result.isConfirmed) {
            copyToClipBoard(`http://localhost:3000/${data.addressContract}/0`);
          }
        });
    }
  };

  const handleConnect = async () => {
    if (typeof window.ethereum !== 'undefined') {
      console.log('MetaMask is installed!');
    }
    const account = await window.ethereum.send('eth_requestAccounts');
    // console.log(account.result[0], '_account')
    setState({ accountConnect: account.result[0] });
  };

  const handleSelectContract = async () => {
    setState({ isConnectContract: !state.isConnectContract });
  };

  const onClickBuy = async () => {
    const accounts = await ethereum.request({ method: 'eth_requestAccounts' });
    const account = accounts[0];
    console.log(account, '__account');
    setState({ dataAccount: account });
    const value = web3.utils.toWei('0.00002', 'ether');
    const rawTransaction = {
      to: '0xF002Dd7F49233CBD40dBBd6D8a79bfED2fA4ACBB',
      from: account,
      data: '',
    };
    const gasPrice = await web3.eth.getGasPrice();
    const gas = await web3.eth.estimateGas(rawTransaction);
    const transactionParameters = {
      to: '0xF002Dd7F49233CBD40dBBd6D8a79bfED2fA4ACBB',
      gasPrice: web3.utils.toHex(gasPrice),
      gas: web3.utils.toHex(gas),
      from: account,
      value: value,
      chainId: '97',
    };
    const txHash = await ethereum.request({
      method: 'eth_sendTransaction',
      params: [transactionParameters],
    });

    if (txHash) {
      setState({ loading: true });
      const ecec = await getContractComplie();
      const { abi, bytecode } = ecec;
      const dataContract = await newDeploy(
        abi,
        bytecode,
        getValues('addressReceive')
      );
      const dataReceipt = async () => {
        web3.eth.getTransactionReceipt(dataContract, (err, receipt) => {
          if (err) {
            console.error(err);
            return;
          }
          if (receipt === null) {
            dataReceipt();
          }
          if (receipt.contractAddress) {
            setValue('addressContract', receipt.contractAddress);
            setState({
              isConnectContract: true,
              addressContract: dataContract,
              loading: false,
            });
          }
        });
      };
      setTimeout(() => {
        dataReceipt();
      }, 100);
    }
  };

  const checkApprol = async () => {
    const okla = await allowance(state.accountConnect, addressContract);
    setIsApproved(okla);
  };

  useEffect(() => {
    checkApprol();
  }, [addressContract]);

  const renderLayout = () => {
    return HOMES.map((layout) => {
      const key = layout.title;

      return (
        <SwiperSlide key={key}>
          <div className='layout-item'>
            <img alt='layout' src={layout.img} className='background' />

            <div className='content'>
              <h2>{layout.title}</h2>

              <p>{layout.isComing ? 'Coming Soon' : ''}</p>
            </div>
          </div>
        </SwiperSlide>
      );
    });
  };

  return (
    <div className='home-container'>
      <main>
        <header>
          <div className='header-container'>
            <section className='flex_content'>
              <strong className='logo'>
                <span className='stroke'>
                  K<span className='stroke rotate'>y</span>
                </span>
                tek<span>❤️</span>
              </strong>
            </section>

            <div className='header-sidebar'>
              <div className='sidebar-item' onClick={openModal}>
                Thêm Layout của bạn
              </div>
            </div>

            {state.accountConnect !== '' ? (
              <button className='btnWallet'>{state.accountConnect}</button>
            ) : (
              <button className='btnWallet' onClick={handleConnect}>
                Kết nối ví
              </button>
            )}
          </div>
        </header>

        <div className='content'>
          <div className={`intro${state.isShowForm ? ' hide-intro' : ''}`}>
            <span>Kytek</span>
            <h1>Chọn ngay</h1>
            <hr />
            <p>
              Hãy chọn cho mình một giao diện và gửi những lời chúc tốt đẹp nhất
              đến bạn bè của bạn
            </p>
            {/* <button onClick={toggleForm}>Gửi ngay</button>
             */}
            <button type='button' onClick={toggleForm}>
              Gửi ngay
            </button>
          </div>

          <div className={`form${!state.isShowForm ? ' hide-form' : ''}`}>
            {state.loading && (
              <div className='containerLoading'>
                <img className='loadingHome' src={loading} alt='loading..' />
              </div>
            )}
            <form action='' onSubmit={handleSubmit(onSubmitForm)}>
              {!state.isConnectContract && (
                <div className='input-field'>
                  <input
                    placeholder='Nhập địa chỉ người nhận'
                    {...register('addressReceive', { required: true })}
                  />
                </div>
              )}
              {errors.addressReceive && (
                <p className='form-errors'>Không được để trống</p>
              )}

              <div className='svg-checkbox'>
                <label className={state.isConnectContract ? 'selected' : ''}>
                  <input
                    name='checkItems'
                    value={state.isConnectContract}
                    type='checkbox'
                    onChange={handleSelectContract}
                  />
                  <span className='checkbox-ghost-2'></span>
                  <svg width='100' height='100'>
                    <g>
                      <circle cx='8' cy='8' r='6'></circle>
                      <circle cx='30' cy='23' r='2'></circle>
                      <circle cx='5' cy='40' r='2'></circle>
                      <circle cx='20' cy='80' r='3'></circle>
                      <circle cx='70' cy='90' r='2'></circle>
                      <circle cx='90' cy='15' r='2'></circle>
                      <circle cx='60' cy='20' r='4'></circle>
                      <circle cx='80' cy='45' r='2'></circle>
                      <circle cx='85' cy='62' r='3'></circle>
                    </g>
                  </svg>
                  Đã có Contract
                </label>
              </div>

              <div
                className={`input-field field-contract${
                  state.isConnectContract ? ' field-active' : ''
                }`}>
                {state.isConnectContract && (
                  <input {...register('addressContract', { required: true })} />
                )}
                {/* <input
                  name='addressContract'
                  id='addressContract'
                  register={}
                  value={state.addressContract}
                  onChange={(e) => setState({ addressContract : e.target.value})}
                  // type='text'
                  // autoComplete='off'
                  placeholder='Nhập địa chỉ contract'
                /> */}
              </div>
              {state.isConnectContract && errors.addressContract && (
                <p className='form-errors'>Không được để trống</p>
              )}

              {state.isConnectContract && (
                <React.Fragment>
                  <div className='input-field'>
                    <input
                      {...register('name', { required: true })}
                      type='text'
                      autoComplete='off'
                      placeholder='Nhập tên của bạn'
                    />
                  </div>

                  <div className='input-field textarea-field'>
                    <textarea
                      rows={5}
                      {...register('loichuc', { required: true })}
                      autoComplete='off'
                      placeholder='Nhập lời chúc của bạn'
                    />
                  </div>
                  <div className='input-field'>
                    <input
                      {...register('tienmung', { required: true })}
                      type='text'
                      autoComplete='off'
                      placeholder='Tiền mừng'
                    />
                  </div>
                </React.Fragment>
              )}
              <button
                type='button'
                style={{ marginRight: 16 }}
                onClick={toggleForm}>
                Quay lại
              </button>

              {state.accountConnect !== '' ? (
                <button type='submit'>
                  {!state.isConnectContract
                    ? 'Mua ngay'
                    : isApproved
                    ? 'Gửi'
                    : 'Approve'}
                </button>
              ) : (
                <button
                  type='button'
                  className='btnWallet'
                  onClick={handleConnect}>
                  Kết nối ví
                </button>
              )}
            </form>
          </div>
        </div>

        <Swiper
          effect={'coverflow'}
          grabCursor={true}
          centeredSlides={true}
          loop={true}
          coverflowEffect={{
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 3,
            slideShadows: true,
          }}
          pagination={{ el: '.swiper-pagination', clickable: true }}
          modules={[EffectCoverflow, Pagination]}
          breakpoints={{
            640: {
              slidesPerView: 2,
            },
            768: {
              slidesPerView: 1,
            },
            1024: {
              slidesPerView: 2,
            },
            1560: {
              slidesPerView: 2,
            },
          }}>
          {renderLayout()}

          {/* <div className='slider-controler'> */}
          <div className='swiper-pagination'></div>
          {/* </div> */}
        </Swiper>

        <img
          src='https://cdn.pixabay.com/photo/2021/11/04/19/39/jellyfish-6769173_960_720.png'
          alt=''
          className='bg'
        />
        <img
          src='https://cdn.pixabay.com/photo/2012/04/13/13/57/scallop-32506_960_720.png'
          alt=''
          className='bg2'
        />
      </main>

      <div
        className={`modal-alert${state.isOpen ? ' open' : ''}`}
        onClick={closeModal}>
        <div className='contentWrapper' onClick={preventClick}>
          <div className='alert-message'>
            <p>Tính năng đang phát triển</p>

            <button type='button' className='btnWallet' onClick={closeModal}>
              Đóng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(Home);
