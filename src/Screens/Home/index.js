import React, { memo, useRef, useState, useEffect } from 'react';

import $ from 'jquery';

import './home.scss';

const HOMES = [
  {
    title: 'Giao diện chúc mừng Sinh Nhật dễ thương',
    isComing: false,
    img: '/home/birthday.jpg',
  },
  {
    title: 'Giao diện Giáng Sinh ấm áp',
    isComing: true,
    img: '/home/christmas.jpeg',
  },
  {
    title: 'Giao diện Trung Thu vui vẻ',
    isComing: true,
    img: '/home/mid_autumn.avif',
  },
  {
    title: 'Giao diện Tết ấm cúm',
    isComing: true,
    img: '/home/new_year.avif',
  },
  {
    title: 'Giao diện Valentine ngọt ngào',
    isComing: true,
    img: '/home/valentine.avif',
  },
];

const Home = () => {
  const [state, set] = useState({
    slide: 0,
    isOpen: false,
    accountConnect: '',
  });

  const setState = (newState) => {
    set((currentState) => ({
      ...currentState,
      ...newState,
    }));
  };

  const openModal = () => setState({ isOpen: true });

  const closeModal = () => setState({ isOpen: false });

  const preventClick = (event) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleConnect = async () => {
    if (typeof window.ethereum !== 'undefined') {
      console.log('MetaMask is installed!');
    }
    const account = await window.ethereum.send('eth_requestAccounts');
    // console.log(account.result[0], '_account')
    setState({ accountConnect: account.result[0] });
  };

  const changeSlide = (index) => () => {
    setState({ slide: index });
  };

  const renderLayout = () => {
    return HOMES.map((layout, index) => {
      const key = layout.title;

      return (
        <li
          key={key}
          className={`l-section section${
            state.slide === index ? ' section--is-active' : ''
          }`}>
          <div className='intro'>
            <div className='intro--banner'>
              {/* <h1>{layout.title}</h1> */}

              <div className='background'>
                <img src={layout.img} alt='layout' />
              </div>

              <button type='button' className={layout.isComing ? 'is-coming' : ''}>
                {layout.isComing ? 'Sắp ra mắt' : 'Gửi ngay'}
              </button>
            </div>
          </div>
        </li>
      );
    });
  };

  const renderNavbar = () => {
    return HOMES.map((layout, index) => {
      const key = layout.title;

      return (
        <li
          key={key}
          onClick={changeSlide(index)}
          className={state.slide === index ? 'is-active' : ''}>
          <span>{layout.title}</span>
        </li>
      );
    });
  };

  return (
    <div className='home-container'>
      <div className='perspective effect-rotate-left'>
        <div className='container'>
          <div id='viewport' className='l-viewport'>
            <div className='l-wrapper'>
              <header className='header'>
                <div className='header-container'>
                  <section className='flex_content'>
                    <strong className='logo'>
                      <span className='stroke'>
                        K<span className='stroke rotate'>y</span>
                      </span>
                      tek<span>❤️</span>
                    </strong>
                  </section>

                  <div className='header-sidebar'>
                    <div className='sidebar-item' onClick={openModal}>
                      Thêm Layout của bạn
                    </div>
                  </div>

                  {state.accountConnect ? (
                    <button className='btnWallet'>
                      {state.accountConnect}
                    </button>
                  ) : (
                    <button className='btnWallet' onClick={handleConnect}>
                      Kết nối ví
                    </button>
                  )}
                </div>
              </header>
            </div>
          </div>
        </div>
      </div>

      <div
        className={`modal-alert${state.isOpen ? ' open' : ''}`}
        onClick={closeModal}>
        <div className='contentWrapper' onClick={preventClick}>
          <div className='alert-message'>
            <p>Tính năng đang phát triển</p>

            <button type='button' className='btnWallet' onClick={closeModal}>
              Đóng
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(Home);
