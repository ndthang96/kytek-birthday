import React, { memo, useState } from 'react';
import { approve, connectWallet, deploy, getContractComplie, getListLoiChuc, getOwner, newDeploy, sendTransaction } from '../../Contract';

const Test = ({ className }) => {


    const [addressNguoiNhan, setAddressNguoiNhan] = useState('')
    const [addressContract, setAddressContract] = useState('')
    const [loiChuc, setLoiChuc] = useState('')
    const [tienMung, setTienMung] = useState(0)
    const [addressRutTien, setAddressRutTien] = useState('')
    const [listLoiChuc, setListLoiChuc] = useState([])

    const [ownerAddress,setOwnerAddress] =useState('')

    const [tx1, setTx1] = useState('')
    const [tx2, setTx2] = useState('')
    const [tx3, setTx3] = useState('')


    const connectMetaMask = async () => {
        await connectWallet()
    }


    const createThiepMung = async () => {
        const ecec = await getContractComplie(); // getAbi from Api
        const { abi, bytecode } = ecec;
        const addressContract = await newDeploy(abi, bytecode,addressNguoiNhan);
        setAddressContract(addressContract)
    }

    const approval = async () => {
        const txHash = await approve(addressContract)
        setTx3(txHash)
    }

    const sendLoiChuc = async () => {
        const ecec = await getContractComplie(); // getAbi from Api
        const { abi, bytecode } = ecec;
        const txHash = await sendTransaction(addressContract, abi, 'loichuc', [loiChuc, tienMung])
        setTx1(txHash)


        //approve cho contract lay tien cua minh


    }

    const withDraw = async () => {
        const ecec = await getContractComplie(); // getAbi from Api
        const { abi, bytecode } = ecec;
        const txHash = await sendTransaction(addressContract, abi, 'withdraw', [addressRutTien])
        setTx2(txHash)
    }

    const getListLoiChucOkla = async () => {
        const ecec = await getContractComplie(); // getAbi from Api
        const { abi, bytecode } = ecec;

        const listLoiChuc = await getListLoiChuc(addressContract,abi)
        setListLoiChuc(listLoiChuc)
    }

    const getOwnerOkla = async () => {
        const ecec = await getContractComplie(); // getAbi from Api
        const { abi, bytecode } = ecec;

        const ownerAddress = await getOwner(addressContract,abi)
        setOwnerAddress(ownerAddress)
    }

    

    console.log(typeof tienMung, tienMung)


    return (
        <div>
            <button onClick={connectMetaMask}>ket noi vi</button>

            <fieldset>
                <label>
                    <p>{`Address nguoi nhan (deploy contract)`}</p>
                    <input value={addressNguoiNhan} onChange={(e) => setAddressNguoiNhan(e.target.value)} />
                </label>
            </fieldset>
            <button onClick={createThiepMung}>Submit</button>
            <br />
            <a target="_blank" href={`https://testnet.bscscan.com/address/${addressContract}`}>{addressContract}</a>

            <br />
            <br />
            <br />

            <button onClick={approval}>approval contract</button>
            <br />
            <a target="_blank" href={`https://testnet.bscscan.com/tx/${tx3}`}>{tx1}</a>


            <fieldset>
                <label>
                    <p>Address contract </p>
                    <input value={addressContract} onChange={(e) => setAddressContract(e.target.value)} />
                </label>
            </fieldset>
            <fieldset>
                <label>
                    <p>loi chuc </p>
                    <input value={loiChuc} onChange={(e) => setLoiChuc(e.target.value)} />
                </label>
            </fieldset>
            <fieldset>
                <label>
                    <p>{`tien mung (USDT)`}</p>
                    <input value={tienMung} onChange={(e) => setTienMung(e.target.value)} />
                </label>
            </fieldset>
            <button onClick={sendLoiChuc}>Submit</button>
            <br />
            <a target="_blank" href={`https://testnet.bscscan.com/tx/${tx1}`}>{tx1}</a>



            <br />
            <br />
            <br />



            <fieldset>
                <label>
                    <p>address rut tien</p>
                    <input value={addressRutTien} onChange={(e) => setAddressRutTien(e.target.value)} />
                </label>
            </fieldset>
            <button onClick={withDraw}>Submit</button>
            <br />
            <a target="_blank" href={`https://testnet.bscscan.com/tx/${tx2}`}>{tx2}</a>


            <br />
            <br />
            <br />

            <fieldset>
                <label>
                    <p>list loi chuc</p>
                </label>
            </fieldset>

            <button onClick={getListLoiChucOkla}>Submit</button>

            <ul>
                {listLoiChuc.map(item=><li>{item}</li>)}
            </ul>

            <fieldset>
                <label>
                    <p>getOwner</p>
                </label>
            </fieldset>

            <button onClick={getOwnerOkla}>Submit</button>

            <br />
            <a target="_blank" href={`https://testnet.bscscan.com/address/${ownerAddress}`}>{ownerAddress}</a>

            

            

        </div>
    );
};

export default memo(Test);
