import React, { memo, useState } from 'react';
import { Outlet } from 'react-router-dom';

import './style.scss';

const BirthdayLayout = ({ className }) => {
  const [state, set] = useState({});

  const setState = (newState) => {
    set((currentState) => ({
      ...currentState,
      ...newState,
    }));
  };

  return (
    <div className='birthday-layout-screen-wrapper'>
      <Outlet />
    </div>
  );
};

export default memo(BirthdayLayout);
