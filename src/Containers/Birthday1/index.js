import React, { memo, useState, useEffect, useRef } from 'react';
import $ from 'jquery';
import { useParams } from 'react-router-dom';
import {
  getOwner,
  getContractComplie,
  getListLoiChuc,
  sendTransaction,
} from '../../Contract';

import balloonBorder from './layout1/Balloon-Border.png';
import banner from './layout1/banner.png';
import audio from './layout1/hbd.mp3';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './style.scss';

let vw;

const Birthday1 = ({ className }) => {
  const timeout = useRef(null);

  const { id, index } = useParams();

  const [state, set] = useState({
    accountConnect: '',
    onwerAddress: false,
    loichuc: '',
    total: 0,
    name: '',
  });

  const setState = (newState) => {
    set((currentState) => ({
      ...currentState,
      ...newState,
    }));
  };

  const handleConnect = async () => {
    if (typeof window.ethereum !== 'undefined') {
      console.log('MetaMask is installed!');
    }
    const account = await window.ethereum.send('eth_requestAccounts');
    if (account.result[0]) {
      console.log('asdasd');
      const data = await getContractComplie(); // getAbi from Api
      const { abi, bytecode } = data;

      const ownerAddress = await getOwner(id, abi);

      console.log(ownerAddress.toLowerCase(), account.result[0]);
      if (ownerAddress.toLowerCase() === account.result[0]) {
        const listLoiChuc = await getListLoiChuc(id, abi);
        console.log(listLoiChuc, '__listLoiChuc');

// <<<<<<< HEAD
        const arrLC = listLoiChuc[index]
        const parse = JSON.parse(arrLC)
        console.log(parse, '__arrLC __arrLCssss')
// =======
//         const arrLC = listLoiChuc.split(/\n/);

// >>>>>>> d7f3e9dc638d7b302aca3d37510892820fd3d7c4
        let lc = '';

        // arrLC.forEach((mess) => {
        //   console.log(mess, '__mess')
        //   if (mess) {
        //     lc = lc + `<p>${mess}</p>`;
        //   }
        // });
        setState({
          name: parse.name,
          loichuc: parse.loichuc,
          onwerAddress: true,
          total: arrLC.length,
          accountConnect: account.result[0],
        });
      } else {
        setState({ accountConnect: account.result[0] });
      }
    }
  };

  const CustomToastWithLink = (txHash) => (
    <div>
      <h5>tx hash</h5>
      <a
        target='_blank'
        href={`https://testnet.bscscan.com/tx/${txHash}`}
        rel='noreferrer'>
        {txHash}
      </a>
    </div>
  );

  const withDraw = async () => {
    const ecec = await getContractComplie(); // getAbi from Api
    const { abi, bytecode } = ecec;
    const txHash = await sendTransaction(id, abi, 'withdraw', [
      window.ethereum.selectedAddress,
    ]);
    toast.info(CustomToastWithLink(txHash));
  };

  const turnOnClick = () => {
    $('#bulb_yellow').addClass('bulb-glow-yellow');
    $('#bulb_red').addClass('bulb-glow-red');
    $('#bulb_blue').addClass('bulb-glow-blue');
    $('#bulb_green').addClass('bulb-glow-green');
    $('#bulb_pink').addClass('bulb-glow-pink');
    $('#bulb_orange').addClass('bulb-glow-orange');
    $('body').addClass('peach');
    $('#turn_on').fadeOut('slow');
    // .delay(2000)
    // .promise()
    // .done(function () {
    //   $('#play').fadeIn('slow');
    // });

    timeout.current = setTimeout(() => {
      play();
    }, 2000);
  };

  const play = () => {
    var audio = $('.song')[0];
    audio.play();
    $('#bulb_yellow').addClass('bulb-glow-yellow-after');
    $('#bulb_red').addClass('bulb-glow-red-after');
    $('#bulb_blue').addClass('bulb-glow-blue-after');
    $('#bulb_green').addClass('bulb-glow-green-after');
    $('#bulb_pink').addClass('bulb-glow-pink-after');
    $('#bulb_orange').addClass('bulb-glow-orange-after');
    $('body').css('backgroud-color', '#FFF');
    $('body').addClass('peach-after');
    // $('#play')
    //   .fadeOut('slow')
    //   .delay(6000)
    //   .promise()
    //   .done(function () {
    //     $('#bannar_coming').fadeIn('slow');
    //   });

    timeout.current = setTimeout(() => {
      decorateClick();
    }, 2000);
  };

  const decorateClick = () => {
    $('.bannar').addClass('bannar-come');
    // $('#bannar_coming')
    //   .fadeOut('slow')
    //   .delay(6000)
    //   .promise()
    //   .done(function () {
    //     $('#balloons_flying').fadeIn('slow');
    //   });

    timeout.current = setTimeout(() => {
      balloonsFlyClick();
    }, 2000);
  };

  const loopOne = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b1').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopOne();
    });
  };

  const loopTwo = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b2').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopTwo();
    });
  };

  const loopThree = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b3').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopThree();
    });
  };

  const loopFour = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b4').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopFour();
    });
  };

  const loopFive = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b5').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopFive();
    });
  };

  const loopSix = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b6').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopSix();
    });
  };

  const loopSeven = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b7').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopSeven();
    });
  };

  const loopEight = () => {
    var randleft = 1000 * Math.random();
    var randtop = 500 * Math.random();
    $('#b8').animate({ left: randleft, bottom: randtop }, 10000, function () {
      loopEight();
    });
  };

  const balloonsFlyClick = () => {
    $('.balloon-border').animate({ top: -500 }, 8000);
    $('#b1,#b4,#b5,#b7').addClass('balloons-rotate-behaviour-one');
    $('#b2,#b3,#b6').addClass('balloons-rotate-behaviour-two');
    // $('#b3').addClass('balloons-rotate-behaviour-two');
    // $('#b4').addClass('balloons-rotate-behaviour-one');
    // $('#b5').addClass('balloons-rotate-behaviour-one');
    // $('#b6').addClass('balloons-rotate-behaviour-two');
    // $('#b7').addClass('balloons-rotate-behaviour-one');
    loopOne();
    loopTwo();
    loopThree();
    loopFour();
    loopFive();
    loopSix();
    loopSeven();
    loopEight();

    // $('#balloons_flying')
    //   .fadeOut('slow')
    //   .delay(2000)
    //   .promise()
    //   .done(function () {
    //     $('#cake_fadein').fadeIn('slow');
    //   });

    timeout.current = setTimeout(() => {
      cakeClick();
    }, 2000);
  };

  const cakeClick = () => {
    $('.cake').fadeIn('slow');
    // $('#cake_fadein')
    //   .fadeOut('slow')
    //   .delay(1500)
    //   .promise()
    //   .done(function () {
    //     $('#light_candle').fadeIn('slow');
    //   });

    timeout.current = setTimeout(() => {
      candleClick();
    }, 2000);
  };

  const candleClick = () => {
    $('.fuego').fadeIn('slow');
    // $('#light_candle')
    //   .fadeOut('slow')
    //   .promise()
    //   .done(function () {
    //     $('#wish_message').fadeIn('slow');
    //   });
    timeout.current = setTimeout(() => {
      // happyBirthdayClick();
      messageClick();
    }, 2000);
  };

  const happyBirthdayClick = () => {
    vw = $(window).width() / 2;

    $('#b1,#b2,#b3,#b4,#b5,#b6,#b7').stop();
    $('#b1').attr('id', 'b11');
    $('#b2').attr('id', 'b22');
    $('#b3').attr('id', 'b33');
    $('#b4').attr('id', 'b44');
    $('#b5').attr('id', 'b55');
    $('#b6').attr('id', 'b66');
    $('#b7').attr('id', 'b77');
    $('#b11').animate({ top: 240, left: vw - 350 }, 500);
    $('#b22').animate({ top: 240, left: vw - 250 }, 500);
    $('#b33').animate({ top: 240, left: vw - 150 }, 500);
    $('#b44').animate({ top: 240, left: vw - 50 }, 500);
    $('#b55').animate({ top: 240, left: vw + 50 }, 500);
    $('#b66').animate({ top: 240, left: vw + 150 }, 500);
    $('#b77').animate({ top: 240, left: vw + 250 }, 500);
    $('.balloons').css('opacity', '0.9');
    $('.balloons h2').fadeIn(1500);
    // $('#wish_message')
    //   .fadeOut('slow')
    //   .delay(1500)
    //   .promise()
    //   .done(function () {
    //     $('#story').fadeIn('slow');
    //   });
  };

  const messageClick = () => {
    $('#story').fadeOut('slow');
    $('.cake')
      .fadeOut('fast')
      .promise()
      .done(function () {
        $('.message').fadeIn('slow');
      });

    // $('.cake').addClass('cake-transform');

    // $('.message').fadeIn('slow');

    let i;

    function msgLoop(i) {
      $('p:nth-child(' + i + ')')
        .fadeOut('slow')
        .delay(500)
        .promise()
        .done(function () {
          i = i + 1;
          $('p:nth-child(' + i + ')')
            .fadeIn('slow')
            .delay(700);
          if (i >= state.total) {
            $(`p:nth-child(${state.total})`)
              .fadeOut('slow')
              .promise()
              .done(function () {
                $('.message')
                  .fadeOut('slow')
                  .promise()
                  .done(function () {
                    showAllMessage();
                  });
                // $('.cake').removeClass('cake-transform');
              });
          } else {
            msgLoop(i);
          }
        });
      // body...
    }

    msgLoop(0);
  };

  const showAllMessage = () => {
    timeout.current = setTimeout(() => {
      $('.message-end').fadeIn('slow');
      document.getElementById('claim').style.opacity = 1;
    }, 500);
  };

  const loadLayout = () => {
    $('document').ready(function () {
      console.log('first');
    });
  };

  useEffect(() => {
    loadLayout();

    window.addEventListener('load', () => {
      $('.loading').fadeOut('fast');
      $('.container').fadeIn('fast');
    });
    window.addEventListener('resize', () => {
      vw = $(window).width() / 2;
      $('#b1,#b2,#b3,#b4,#b5,#b6,#b7').stop();
      $('#b11').animate({ top: 240, left: vw - 350 }, 500);
      $('#b22').animate({ top: 240, left: vw - 250 }, 500);
      $('#b33').animate({ top: 240, left: vw - 150 }, 500);
      $('#b44').animate({ top: 240, left: vw - 50 }, 500);
      $('#b55').animate({ top: 240, left: vw + 50 }, 500);
      $('#b66').animate({ top: 240, left: vw + 150 }, 500);
      $('#b77').animate({ top: 240, left: vw + 250 }, 500);
    });

    return () => {
      clearTimeout(timeout.current);
      window.removeEventListener('load', () => {
        $('.loading').fadeOut('fast');
        $('.container').fadeIn('fast');
      });

      window.removeEventListener('resize', () => {
        vw = $(window).width() / 2;
        $('#b1,#b2,#b3,#b4,#b5,#b6,#b7').stop();
        $('#b11').animate({ top: 240, left: vw - 350 }, 500);
        $('#b22').animate({ top: 240, left: vw - 250 }, 500);
        $('#b33').animate({ top: 240, left: vw - 150 }, 500);
        $('#b44').animate({ top: 240, left: vw - 50 }, 500);
        $('#b55').animate({ top: 240, left: vw + 50 }, 500);
        $('#b66').animate({ top: 240, left: vw + 150 }, 500);
        $('#b77').animate({ top: 240, left: vw + 250 }, 500);
      });
    };
  }, []);

  return (
    <div className='birthday-1-wrapper'>
      <audio className='song' controls loop>
        <source src={audio} />
        Your browser isn't invited for super fun audio time.
      </audio>
      <div className='balloons text-center' id='b1'>
        <h2 style={{ color: '#F2B300' }}>H</h2>
      </div>
      <div className='balloons text-center' id='b2'>
        <h2 style={{ color: '#0719D4' }}>B</h2>
      </div>
      <div className='balloons text-center' id='b3'>
        <h2 style={{ color: '#D14D39' }}>D</h2>
      </div>
      <div className='balloons text-center' id='b4'>
        <h2 style={{ color: '#8FAD00' }}>K</h2>
      </div>
      <div className='balloons text-center' id='b5'>
        <h2 style={{ color: '#8377E4' }}>Y</h2>
      </div>
      <div className='balloons text-center' id='b6'>
        <h2 style={{ color: '#99C96A' }}>T</h2>
      </div>
      <div className='balloons text-center' id='b7'>
        <h2 style={{ color: '#20CFB4' }}>E</h2>
      </div>
      <div className='balloons text-center' id='b8'>
        <h2 style={{ color: '#20CFB4' }}>K</h2>
      </div>

      <img src={balloonBorder} width='100%' className='balloon-border' />

      <div className='container'>
        <div className='row'>
          <div className='col-md-2 col-xs-2 bulb-holder'>
            <div className='bulb' id='bulb_yellow'></div>
          </div>
          <div className='col-md-2 col-xs-2 bulb-holder'>
            <div className='bulb' id='bulb_red'></div>
          </div>
          <div className='col-md-2 col-xs-2 bulb-holder'>
            <div className='bulb' id='bulb_blue'></div>
          </div>
          <div className='col-md-2 col-xs-2 bulb-holder'>
            <div className='bulb' id='bulb_green'></div>
          </div>
          <div className='col-md-2 col-xs-2 bulb-holder'>
            <div className='bulb' id='bulb_pink'></div>
          </div>
          <div className='col-md-2 col-xs-2 bulb-holder'>
            <div className='bulb' id='bulb_orange'></div>
          </div>
        </div>
        <div className='row'>
          <div className='col-md-12 text-center'>
            <img src={banner} className='bannar' />
          </div>
        </div>
        <div className='row cake-cover'>
          <div className='col-md-12 texr-center'>
            <div className='cake'>
              <div className='velas'>
                <div className='fuego'></div>
                <div className='fuego'></div>
                <div className='fuego'></div>
                <div className='fuego'></div>
                <div className='fuego'></div>
              </div>
              <div className='cobertura'></div>
              <div className='bizcocho'></div>
            </div>
          </div>
        </div>
        <div className='row message'>
          <div
            className='col-md-12'
            dangerouslySetInnerHTML={{ __html: `${state.name} :` }}
          />
        </div>

        <div className='row message-end'>
          <div
            className='col-md-12'
            dangerouslySetInnerHTML={{ __html: state.loichuc }}
          />

          <div className='col-md-12'>
            <p className='user'>{state.name}</p>
          </div>
        </div>

        <div className='claim' id='claim'>
          <button type='button' onClick={withDraw}>
            Nhận tiền mừng
          </button>
        </div>

        <div className='action-center'>
          {state.accountConnect === '' ? (
            <button id='wallet-connect' onClick={handleConnect} />
          ) : state.onwerAddress ? (
            <button id='turn_on' onClick={turnOnClick} />
          ) : (
            <div className='error-owner'>
              Xin lỗi!! Lời chúc này không dành cho bạn 😢
            </div>
          )}
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default memo(Birthday1);
