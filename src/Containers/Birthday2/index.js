import React, { memo, useState } from 'react';

const Birthday2 = ({ className }) => {
  const [state, set] = useState({});

  const setState = (newState) => {
    set((currentState) => ({
      ...currentState,
      ...newState,
    }));
  };

  return <div className='birthday-2-wrapper'>birthday 2</div>;
};

export default memo(Birthday2);
