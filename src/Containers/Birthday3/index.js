import React, { memo, useState } from 'react';
import './index.css';
import {
  getOwner,
  getContractComplie,
  getListLoiChuc
} from '../../Contract';
import { useParams } from 'react-router-dom';

const Birthday3 = ({ className }) => {
  let { id } = useParams();

  const [state, set] = useState({
    accountConnect: '',
    onwerAddress: false,
    loichuc: '',
  });

  const setState = (newState) => {
    set((currentState) => ({
      ...currentState,
      ...newState,
    }));
  };

  const handleConnect = async () => {
    if (typeof window.ethereum !== 'undefined') {
      console.log('MetaMask is installed!');
    }
    const account = await window.ethereum.send('eth_requestAccounts');
    setState({ accountConnect: account.result[0] });
    if(account.result[0]){
      console.log('asdasd')
      const ecec = await getContractComplie(); // getAbi from Api
      const { abi, bytecode } = ecec;

      const ownerAddress = await getOwner(id,abi)
      if(ownerAddress.toLowerCase() == account.result[0]){
        const listLoiChuc = await getListLoiChuc(id,abi)
        console.log(listLoiChuc,'__listLoiChuc')
        setState({ loichuc : listLoiChuc})
        setState({ onwerAddress : true })
      }
    }
  };

  return <div className='birthday-2-wrapper'>
    {
      state.accountConnect === ''
      ?
      <button className='btnWallet1' onClick={handleConnect}>
        Kết nối ví
      </button>
      :
      state.onwerAddress &&
        <div class="card3">
            <div class="imgBox">
              <div class="bark"></div>
              <img src="https://image.ibb.co/fYzTrb/lastofus.jpg"/>
            </div>
            <div class="details">
              <h4 class="color1">You're not a Fossil! (YET)</h4>
              <h4 class="color2 margin">(HAPPY BIRTHDAY)</h4>
              <br></br>
              <p class="text-right">{state.loichuc[0]}</p>
              {/* <p class="text-right">♥Sarah</p> */}
            </div>
        </div>
    }
  </div>
};

export default memo(Birthday3);
